# Manage a TypeScript-JavaScript monorepo with pnpm

This guide will setup a TypeScript-JavaScript monorepo with pnpm in order to manage multiple packages with their dependencies.

A _root_ workspace will manage _app1_ and _app2_ packages. The two packages will have common and specific dependencies.

## Relative works

- [Manage a TypeScript-JavaScript monorepo with npm](https://gitlab.com/artios-org/artios-testbedding/manage-a-typescript-javascript-monorepo-with-npm)

  A key difference between the pnpm and npm terminology is the term "workspaces/packages".

  In pnpm, the root directory defines the workspace with all directories containing Node.js projects.

  In npm, every directory containing a Node.js project is called a workspace.

## Prerequisites

- [Node.js](https://nodejs.org/) must be installed - Check the [nvm](https://gitlab.com/artios-org/artios-testbedding/manage-node.js-and-npm-versions-with-nvm) guide to manage Node.js and npm versions;
- [pnpm](https://pnpm.io/) must be installed.

## Guide

_Highly inspired by the [workspaces - pnpm.io](https://pnpm.io/workspaces), [.npmrc - pnpm.io](https://pnpm.io/npmrc) and [pnpm-workspace.yaml](https://pnpm.io/pnpm-workspace_yaml) guides._

### Initialize the root directoy

Initialize the root directory.

```sh
# Initialize the root workspace
pnpm init
```

This will generate a [`package.json`](./package.json) file with all the definition for the root directory.

Add the following script to the [`package.json`](./package.json) file. It will ensure only pnpm is used to install the dependencies.

```json
{
  // [...]
  "scripts": {
    "preinstall": "npx only-allow pnpm"
  },
  // [...]
}
```

### Create and configure the workspace

The [`apps`](./apps/) is directory contains all the applications that we develop.

Configure the apps' directories with the following commands.

```sh
# Create the first app directory
mkdir -p ./apps/app1

# Switch to the first app directory
cd ./apps/app1

# Initialize the first app
pnpm init

# Switch back to the root directory
cd ../..

# Create the second app directory
mkdir -p ./apps/app2

# Switch to the second app directory
cd ./apps/app2

# Initialize the second app
pnpm init

# Switch back to the root directory
cd ../..
```

This will generate a [`package.json`](./apps/app1/package.json) file in the `apps/app1` directory and a [`package.json`](./apps/app2/package.json) in the `apps/app2` directory.

The two files are the definition of the apps' Node.js projects.

Add the following script to both `package.json` ([`package.json`](./apps/app1/package.json) and [`package.json`](./apps/app2/package.json)) files. It will ensure only pnpm is used to install the dependencies.

```json
{
  // [...]
  "scripts": {
    "preinstall": "npx only-allow pnpm"
  },
  // [...]
}
```

Create a [`pnpm-workspace.yaml`](./pnpm-workspace.yaml) file for the workspace definition.

Create a [`.npmrc`](./.npmrc) file to tune pnpm configuration.

### Run commands in the workspace

Now that the workspace has been setup, commands can be ran for some or all of the directories. These are examples.

```sh
# For Node.js project `@monorepo-with-pnpm/app1` run `start` script
pnpm --filter @monorepo-with-pnpm/app1 start

# For Node.js projects `@monorepo-with-pnpm/app1` and `@monorepo-with-pnpm/app2` run `start` script in parallel
pnpm --parallel --filter @monorepo-with-pnpm/app1 --filter @monorepo-with-pnpm/app2 start

# Run `start` script for all sub-Node.js projects from the current directory
pnpm --filter "*" start

# Run `start` script for all Node.js projects from the root directory
pnpm --workspace-root --filter "*" start

# Run `start` script for all Node.js projects from the root directory
pnpm --workspace-root --filter "*" --if-present start
```

### Install and/or update a common dependency

[Prettier](https://prettier.io/) allows to format a codebase with a common set of rules to have concistency across the different styles that different programmers can use.

Install Prettier for the root Node.js project (`@monorepo-with-pnpm/root`).

```sh
# Install Prettier
pnpm install --workspace-root --save-dev prettier
```

The [`pnpm-lock.yaml`](./pnpm-lock.yaml) will be created containing the exact version of Prettier installed for the root workspace.

Create the [`.prettierrc.js`](./.prettierrc.js) file with all the common rules for the entire codebase.

Add the following scripts to the [`package.json`](./package.json) file. These scripts will run Prettier to validate and fix code format issues for `js` and `json` files.

```json
{
  // [...]
  "scripts": {
    // [...]
    "review:code:format": "prettier --check \"*.{js,json}\" --ignore-path .gitignore",
    "review:code:format:fix": "prettier --write \"*.{js,json}\" --ignore-path .gitignore"
  },
  // [...]
}
```

The root directory can now be formatted using the command `pnpm review:code:format` from the `@monorepo-with-pnpm/root` Node.js project directory.

### Install and/or update a specific dependency

We will now install specific packages for each of our applications.

The first app will display a random animal name using the [random-animal-name](https://www.npmjs.com/package/random-animal-name) library.

The second app will display a random emoji using the [@sefinek/random-emoji](https://www.npmjs.com/package/@sefinek/random-emoji) library with TypeScript.

#### App 1

In the [`apps/app1`](./apps/app1) directory, install the dependency.

```sh
# Install the production dependencies
pnpm install random-animal-name

# Install the dev dependencies - The Prettier version will align with the workspace version
pnpm install --save-dev prettier
```

Create the [`main.js`](./apps/app1/main.js) file and add the following scripts to the [`package.json`](./apps/app1/package.json) file. These scripts will run Prettier to validate and fix code format issues for `js` and `json` files in any directories and sub-directories of the workspace and run the application as well.

```json
{
  // [...]
  "scripts": {
    // [...]
    "review:code:format": "prettier --check \"**/*.{js,json}\" --ignore-path .gitignore",
    "review:code:format:fix": "prettier --write \"**/*.{js,json}\" --ignore-path .gitignore",
    "start": "node main.js"
  },
  // [...]
}
```

A random animal name will be displayed on the screen when running `pnpm start` from the `@monorepo-with-pnpm/app1` Node.js project directory or `pnpm --filter @monorepo-with-pnpm/app1 start` from anywhere in the workspace.

#### App 2

In the [`apps/app2`](./apps/app2) directory, install the dependencies.

```sh
# Install the production dependencies
pnpm install @sefinek/random-emoji

# Install the dev dependencies - The Prettier version will align with the workspace version
pnpm install --save-dev typescript prettier
```

Create the [`main.ts`](./apps/app2/main.ts) file and add the following scripts to the [`package.json`](./apps/app2/package.json) file. These scripts will run Prettier to validate and fix code format issues for `js` and `json` files in any directories and sub-directories of the workspace, build the application with TypeScript and run the application as well.

```json
{
  // [...]
  "scripts": {
    // [...]
    "review:code:format": "prettier --check \"**/*.{json,ts}\" --ignore-path .gitignore",
    "review:code:format:fix": "prettier --write \"**/*.{json,ts}\" --ignore-path .gitignore",
    "build": "tsc",
    "start": "node dist/main.js"
  },
  // [...]
}
```

A random animal name will be displayed on the screen when running  `pnpm start` from the `@monorepo-with-pnpm/app2` Node.js project directory or `pnpm --filter @monorepo-with-pnpm/app2 start` from anywhere in the workspace.

### Run the entire pipeline

Run the pipeline to ensure all applications works as expected.

```sh
# Validate the code format
pnpm --workspace-root --parallel --filter "*" review:code:format

# Fix the code format
pnpm --workspace-root --parallel --filter "*" review:code:format:fix

# Build all applications (if applicable)
pnpm --workspace-root --parallel --if-present --filter "*" build

# Run all applications
pnpm --workspace-root --filter "*" start
```

All Node.js projects should check code format. Both applications should display a random name and a random emoji on start.

### Create a `pnpm-lock.yaml` file for a specific package

In some specific conditions (in a CI/CD pipeline for example), it might be needed to generate a `pnpm-lock.yaml` file for a specific package so it can be deployed independently for the current workspace.

In the Node.js project directory for which a dedicated `pnpm-lock.yaml` file is required, run the following command.

```sh
# Create the declaration `pnpm-lock.yaml`
pnpm --package @pnpm/make-dedicated-lockfile dlx make-dedicated-lockfile
```

**Note**: It might change the `package.json` file with unwanted changes. Be careful when using this command.

At the time of writing this guide, a new experimental [pnpm deploy](https://pnpm.io/cli/deploy) command is available. However, it seems this command does not generate a dedicated `package-lock.json` file for the given Node.js project. This can lead to unexpected behavior so we stick with the above method instead.

## Additional resources

- _None at the moment._

## License

This work is licensed under the [Non-Profit Open Software License version 3.0](https://opensource.org/licenses/NPOSL-3.0).
